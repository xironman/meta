## Synopsis

Metabuild is a repository manager for the Ironman project.
It clones and builds repositories into opkg's, tags repositories
and prepares changelogs for Ironman releases.

## Installation

Metabuild is based on GNU Make.  It is designed to build for the following 
hardware platforms:

    Raspberry Pi (PiBox)

Before proceeding, read any Linux distribution-specific notes, such as 
"fedora.notes".  If there are none, then don't worry about it.

The build depends on the cdtools environment setup.  The environment
file is in scripts/meta.sh.  See cdtools documentation for it's use.
See cdtools:  http://www.graphics-muse.org/wiki/pmwiki.php/Cdtools/Cdtools

After editing meta.sh, source the script:

    . cdtools

Then setup your environment by running the function in that script:

    meta

Now you're ready to retrieve the source.  Run the following command to see
how to clone the source tree:

    cd?

This will tell you exactly what to do.  After you clone the source you can
do perform package builds with the build.sh script.  To get help, use the
following command.

    ./build.sh -?

To generate a release changelog, including tagging repositories, use the 
MakeChangelong.sh script.  To get help, use the following command.

    ./MakeChangelog.sh -?

## Contributors

To get involved with PiBox and Ironman, contact the project administrator:
Michael J. Hammel <mjhammel@graphics-muse.org>

## License

MIT 
